﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass.NHibernate
{
    /// <summary>
    /// Environment for NHibernate data mapping mechanism
    /// </summary>
    public class NEnvironment : IDGEnvironment
    {
        [ThreadStatic]
        private static IDGUnitOfWork _unitOfWork;

        #region IDGEnvironment Members

        public string MechanismName
        {
            get { return "NHibernate"; }
        }

        public string MechanismDllName
        {
            get { return "DataGlass.NHibernate.dll"; }
        }
                
        public IDGUnitOfWork GetUnitOfWork()
        {
            return this.GetUnitOfWork(null);
        }

        public IDGUnitOfWork GetUnitOfWork(IDGUnitOfWorkSetup unitOfWorkSetup)
        {
            if (_unitOfWork == null)
                _unitOfWork = new NUnitOfWork(unitOfWorkSetup);

            return _unitOfWork;
        }

        #endregion
    }
}
