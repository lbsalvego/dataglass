﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;

namespace DataGlass.NHibernate
{
    public class NQueryCriteriaVisitor<T> : IDGQueryCriteriaVisitor<T> where T : class
    {
        #region Private Fields

        private ISession _session;
        private ICriteria _criteria;
        
        #endregion

        #region Private Methods

        private void Visit(List<IDGQueryCriteria<T>> conjunctionInnerCriterias, List<IDGQueryCriteria<T>> disjunctionInnerCriterias, List<IDGQueryPredicate> conjunctions, List<IDGQueryPredicate> disjunctions, List<IDGQueryOrder> orders)
        {
            //Conjunction Inner Criterias
            foreach (var conjunctionCriteria in conjunctionInnerCriterias)
            {
                conjunctionCriteria.Accept(this);
            }

            //Disjunction Inner Criterias
            foreach (var disjunctionCriteria in disjunctionInnerCriterias)
            {
                disjunctionCriteria.Accept(this);
            }

            //Conjunction predicates
            foreach (var conjunction in conjunctions)
            {
                switch (conjunction.Operator)
                {
                    case PredicateOperator.Equals:
                        _criteria.Add(Restrictions.Eq(conjunction.Field, conjunction.Value));
                        break;
                    case PredicateOperator.NotEquals:
                        _criteria.Add(Restrictions.Not(Restrictions.Eq(conjunction.Field, conjunction.Value)));
                        break;
                    case PredicateOperator.Greater:
                        _criteria.Add(Restrictions.Gt(conjunction.Field, conjunction.Value));
                        break;
                    case PredicateOperator.GreaterOrEquals:
                        _criteria.Add(Restrictions.Ge(conjunction.Field, conjunction.Value));
                        break;
                    case PredicateOperator.Less:
                        _criteria.Add(Restrictions.Lt(conjunction.Field, conjunction.Value));
                        break;
                    case PredicateOperator.LessOrEquals:
                        _criteria.Add(Restrictions.Le(conjunction.Field, conjunction.Value));
                        break;
                    case PredicateOperator.Like:
                        _criteria.Add(Restrictions.Like(conjunction.Field, conjunction.Value));
                        break;
                    default:
                        break;
                }
            }

            //Disjunction predicates
            if (disjunctions.Count > 0)
            {
                var disjunctionElement = Restrictions.Disjunction();
                foreach (var disjunction in disjunctions)
                {
                    switch (disjunction.Operator)
                    {
                        case PredicateOperator.Equals:
                            disjunctionElement.Add(Restrictions.Eq(disjunction.Field, disjunction.Value));
                            break;
                        case PredicateOperator.NotEquals:
                            disjunctionElement.Add(Restrictions.Not(Restrictions.Eq(disjunction.Field, disjunction.Value)));
                            break;
                        case PredicateOperator.Greater:
                            disjunctionElement.Add(Restrictions.Gt(disjunction.Field, disjunction.Value));
                            break;
                        case PredicateOperator.GreaterOrEquals:
                            disjunctionElement.Add(Restrictions.Ge(disjunction.Field, disjunction.Value));
                            break;
                        case PredicateOperator.Less:
                            disjunctionElement.Add(Restrictions.Lt(disjunction.Field, disjunction.Value));
                            break;
                        case PredicateOperator.LessOrEquals:
                            disjunctionElement.Add(Restrictions.Le(disjunction.Field, disjunction.Value));
                            break;
                        case PredicateOperator.Like:
                            disjunctionElement.Add(Restrictions.Like(disjunction.Field, disjunction.Value));
                            break;
                        default:
                            break;
                    }
                }
                _criteria.Add(disjunctionElement);
            }

            //Order by
            foreach (var order in orders)
            {
                switch (order.Operator)
                {
                    case OrderOperator.Ascending:
                        _criteria.AddOrder(Order.Asc(order.Field));
                        break;
                    case OrderOperator.Descending:
                        _criteria.AddOrder(Order.Desc(order.Field));
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion

        #region Public Methods

        public NQueryCriteriaVisitor(ISession session)
        {
            _session = session;
            _criteria = _session.CreateCriteria<T>();
        }

        public IList<T> GetByQuery()
        {
            return _criteria.List<T>();
        }

        #endregion
   
        #region IDGQueryCriteriaVisitor<T> Members

        public void VisitCriteria(List<IDGQueryCriteria<T>> conjunctionInnerCriterias, List<IDGQueryCriteria<T>> disjunctionInnerCriterias, List<IDGQueryPredicate> conjunctions, List<IDGQueryPredicate> disjunctions, List<IDGQueryOrder> orders)
        {
            this.Visit(conjunctionInnerCriterias, disjunctionInnerCriterias, conjunctions, disjunctions, orders); 
        }

        #endregion
    }
}
