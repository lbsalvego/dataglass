﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGlass.NHibernate
{
    public class NUnitOfWorkSetup : IDGUnitOfWorkSetup
    {
        #region Private Fields

        private bool _isSet;

        private ISessionFactory _sessionFactory;

        #endregion

        #region Public Properties

        public ISessionFactory SessionFactory
        {
            get { return _sessionFactory; }
        }

        #endregion

        #region Virtual Methods

        protected virtual void Configure()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDGUnitOfWorkSetup

        public bool IsSet
        {
            get { return _isSet; }
        }

        public void Setup()
        {
            this.Configure();
        }

        #endregion 
    }
}
