﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass.NHibernate
{
    /// <summary>
    /// Environment for NHibernate data mapping mechanism
    /// </summary>
    public class Environment : IEnvironment
    {
        [ThreadStatic]
        private static IUnitOfWork _unitOfWork;

        #region IEnvironment Members

        public string MechanismName
        {
            get { return "NHibernate"; }
        }

        public string MechanismDllName
        {
            get { return "DataGlass.NHibernate.dll"; }
        }

        public IUnitOfWork GetUnitOfWork()
        {
            if (_unitOfWork == null)
                _unitOfWork = new NUnitOfWork();
            
            return _unitOfWork;
        }

        #endregion
    }
}
