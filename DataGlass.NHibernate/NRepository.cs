﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Linq;

namespace DataGlass.NHibernate
{
    internal class NRepository<T> : IDGRepository<T> where T : class
    {
        #region Private Fields

        ISession _session;

        #endregion

        #region Public Methods

        public NRepository(ISession session)
        {
            _session = session;
        }

        #endregion

        #region IDGRepository<T> Members

        public T GetById(int id)
        {
            return _session.Get<T>(id);
        }

        public T GetById(object objectId)
        {
            return _session.Get<T>(objectId);
        }

        public T LoadById(int id)
        {
            return _session.Load<T>(id);
        }

        public T LoadById(object objectId)
        {
            return _session.Load<T>(objectId);
        }

        public IList<T> GetByStringQuery(string query, params object[] parameters)
        {
            return _session.CreateQuery(string.Format(query, parameters)).List<T>();
        }

        public IList<T> GetByDGQuery(IDGQueryCriteria<T> query)
        {
            var visitor = new NQueryCriteriaVisitor<T>(_session);
            query.Accept(visitor);
            return visitor.GetByQuery();
        }

        public IList<T> GetByNamedQuery(string queryName, params object[] parameters)
        {
            var query = _session.GetNamedQuery(queryName);
            for (int i = 0; i < query.NamedParameters.Count() - 1; i++)
            {
                query.SetParameter(i, parameters[i]);
            }
            return query.List<T>();
        }

        public IQueryable<T> GetQueryable()
        {
            throw new NotSupportedException();
        }

        public IQueryable<T> GetLazyQueryable()
        {
            return _session.Query<T>();
        }

        public void Save(T entity)
        {
            _session.Save(entity);
        }

        public void SaveOrUpdate(T entity)
        {
            _session.SaveOrUpdate(entity);
        }

        public void Delete(T entity)
        {
            _session.Delete(entity);
        }

        public void Evict(T entity)
        {
            _session.Evict(entity);
        }

        public void Detach(T entity)
        {
            throw new NotSupportedException();
        }

        public void Attach(T entity)
        {
            _session.Update(entity);
        }

        public bool Contains(T entity)
        {
            return _session.Contains(entity);
        }

        public void Refresh(T entity)
        {
            _session.Refresh(entity);
        }

        #endregion
    }
}
