﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Cfg;
using System.Data;
using DataGlass.Settings;

namespace DataGlass.NHibernate
{
    internal class NUnitOfWork : IDGUnitOfWork
    {
        #region Private Fields
        
        private bool _rollbackOcurred = false;

        private int _transactionCounter = 0;

        private ITransaction _transaction;

        private ISession _session;

        private ISessionFactory _sessionFactory;

        #endregion

        #region Private Methods

        private ISessionFactory GetSessionFactory(IDGUnitOfWorkSetup unitOfWorkSetup)
        {
            if (unitOfWorkSetup != null)
            {
                if ((unitOfWorkSetup as NUnitOfWorkSetup) == null)
                    throw new InvalidCastException("Unit Of Work Setup is not a NUnitOfWork object.");

                if (!unitOfWorkSetup.IsSet)
                    unitOfWorkSetup.Setup();

                _sessionFactory = (unitOfWorkSetup as NUnitOfWorkSetup).SessionFactory;
            }
                        
            if (_sessionFactory == null)
            {
                var configuration = new Configuration();
                configuration.Configure();
                _sessionFactory = configuration.BuildSessionFactory();
            }
            
            return _sessionFactory;
        }

        #endregion

        #region Public Methods

        public NUnitOfWork(IDGUnitOfWorkSetup unitOfWorkSetup)
        {
            _sessionFactory = this.GetSessionFactory(unitOfWorkSetup);
        }

        #endregion

        #region IUnitOfWork Members

        public int TransactionCounter
        {
            get { return _transactionCounter; }
        }

        public void BeginTransaction()
        {
            this.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            if (_transaction == null)
            {
                try
                {
                    _session = _sessionFactory.OpenSession();
                    _session.FlushMode = FlushMode.Commit;
                    _transaction = _session.BeginTransaction(isolationLevel);
                    _rollbackOcurred = false;
                }
                catch (Exception ex)
                {
                    try
                    {
                        _session.Close();
                        _session.Dispose();
                    }
                    catch
                    { }
                    _transaction = null;
                    _transactionCounter = 0;
                    throw new ApplicationException("Database or connection error during BeginTransaction.", ex); 
                }
            }
            _transactionCounter += 1;
        }

        public void Commit()
        {
            if (_transactionCounter < 1)
            {
                throw new ApplicationException("The Unit Of Work has no transactions opened.");
            }

            if (_transactionCounter - 1 == 0)
            {
                if (_rollbackOcurred)
                {
                    throw new ApplicationException("A rollback occurred inside a nested transaction.");
                }
                else
                {
                    try
                    {
                        _transaction.Commit();
                        _transaction.Dispose();
                        _transaction = null;
                        _session.Close();
                        _session.Dispose();
                        _transactionCounter = 0;
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            _session.Close();
                            _session.Dispose();
                        }
                        catch
                        { }
                        _transaction = null;
                        _transactionCounter = 0;
                        throw new ApplicationException("Database or connection error during Commit.", ex);
                    }
                }
            }
            else
            {
                _transactionCounter -= 1;
            }
        }

        public void Rollback()
        {
            if (_transactionCounter < 1)
            {
                throw new ApplicationException("The Unit Of Work has no transactions opened.");
            }

            _rollbackOcurred = true;
            _transaction.Rollback();

            if (_transactionCounter - 1 == 0)
            {
                try
                {   
                    _transaction.Dispose();
                    _transaction = null;
                    _session.Close();
                    _session.Dispose();
                    _transactionCounter = 0;
                }
                catch (Exception ex)
                {
                    try
                    {
                        _session.Close();
                        _session.Dispose();
                    }
                    catch
                    { }
                    _transaction = null;
                    _transactionCounter = 0;
                    throw new ApplicationException("Database or connection error during Rollback.", ex);
                }
            }
            else
            {
                _transactionCounter -= 1;
            }
        }

        public IDbConnection GetConnection()
        {
            if (_transactionCounter < 1)
            {
                throw new ApplicationException("The Unit Of Work has no transactions opened.");
            }

            return _session.Connection;
        }

        public void Enlist(System.Data.IDbCommand command)
        {
            if (_transactionCounter < 1)
            {
                throw new ApplicationException("The Unit Of Work has no transactions opened.");
            }

            command.Connection = _session.Connection;

            _transaction.Enlist(command);
        }

        public void ClearCache()
        {
            if (_transactionCounter < 1)
            {
                throw new ApplicationException("The Unit Of Work has no transactions opened.");
            }

            _session.Clear();
        }

        public IDGRepository<T> GetRepository<T>() where T : class
        {
            if (_transactionCounter < 1)
            {
                throw new ApplicationException("The Unit Of Work has no transactions opened.");
            }

            return new NRepository<T>(_session);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
            }

            if (_session != null)
            {
                _session.Close();
                _session.Dispose();
            }

            if (_sessionFactory != null)
            {
                _sessionFactory.Close();
                _sessionFactory.Dispose();
            }
        }

        #endregion
    }
}
