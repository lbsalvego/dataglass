﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass
{
    public class DGQueryOrder : IDGQueryOrder
    {
        #region Private Fields

        private string _field;
        private OrderOperator _operator;

        #endregion

        #region Private Methods

        private DGQueryOrder(string field, OrderOperator orderOperator)
        {
            _field = field;
            _operator = orderOperator;
        }

        #endregion

        #region Public Methods

        public static IDGQueryOrder Ascending(string field)
        {
            return new DGQueryOrder(field, OrderOperator.Ascending);
        }

        public static IDGQueryOrder Descending(string field)
        {
            return new DGQueryOrder(field, OrderOperator.Descending);
        }

        #endregion

        #region IQueryOrder Members

        public string Field
        {
            get { return _field; }
        }

        public OrderOperator Operator
        {
            get { return _operator; }
        }

        #endregion
    }
}
