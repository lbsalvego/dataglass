﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass
{
    public class DGQueryCriteria<T> : IDGQueryCriteria<T> where T : class
    {
        #region Private Fields

        List<IDGQueryCriteria<T>> _conjuntionInnerCriteriaList;

        List<IDGQueryCriteria<T>> _disjuntionInnerCriteriaList;

        List<IDGQueryPredicate> _conjunctionList;

        List<IDGQueryPredicate> _disjunctionList;

        List<IDGQueryOrder> _orderList;

        #endregion

        #region Public Methods

        public DGQueryCriteria()
        {
            _conjuntionInnerCriteriaList = new List<IDGQueryCriteria<T>>();
            _disjuntionInnerCriteriaList = new List<IDGQueryCriteria<T>>();
            _conjunctionList = new List<IDGQueryPredicate>();
            _disjunctionList = new List<IDGQueryPredicate>();
            _orderList = new List<IDGQueryOrder>();
        }

        #endregion

        #region IQueryCriteria<T> Members

        public IDGQueryCriteria<T> AddConjunctionQueryCriteria(IDGQueryCriteria<T> queryCriteria)
        {
            if (queryCriteria == null)
                throw new ArgumentNullException();

            _conjuntionInnerCriteriaList.Add(queryCriteria);

            return this;
        }

        public IDGQueryCriteria<T> AddDisjunctionQueryCriteria(IDGQueryCriteria<T> queryCriteria)
        {
            if (queryCriteria == null)
                throw new ArgumentNullException();

            _disjuntionInnerCriteriaList.Add(queryCriteria);

            return this;
        }

        public IDGQueryCriteria<T> AddConjunctionPredicate(IDGQueryPredicate predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException();

            _conjunctionList.Add(predicate);

            return this;
        }

        public IDGQueryCriteria<T> AddDisjunctionPredicate(IDGQueryPredicate predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException();

            _disjunctionList.Add(predicate);

            return this;
        }
                
        public IDGQueryCriteria<T> AddOrder(IDGQueryOrder order)
        {
            if (order == null)
                throw new ArgumentNullException();
                        
            _orderList.Add(order);

            return this;
        }

        public void Accept(IDGQueryCriteriaVisitor<T> visitor)
        {
            visitor.VisitCriteria(this._conjuntionInnerCriteriaList,
                this._disjuntionInnerCriteriaList,
                this._conjunctionList, 
                this._disjunctionList, 
                this._orderList);
        }

        #endregion
    }
}
