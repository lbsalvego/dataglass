﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass
{
    public class QueryCriteria<T> : IQueryCriteria<T> where T : class
    {
        #region Private Fields

        List<IQueryCriteria<T>> _conjuntionInnerCriteriaList;

        List<IQueryCriteria<T>> _disjuntionInnerCriteriaList;

        List<IQueryPredicate> _conjunctionList;

        List<IQueryPredicate> _disjunctionList;

        List<IQueryOrder> _orderList;

        #endregion

        #region Public Methods

        public QueryCriteria()
        {
            _conjuntionInnerCriteriaList = new List<IQueryCriteria<T>>();
            _disjuntionInnerCriteriaList = new List<IQueryCriteria<T>>();
            _conjunctionList = new List<IQueryPredicate>();
            _disjunctionList = new List<IQueryPredicate>();
            _orderList = new List<IQueryOrder>();
        }

        #endregion

        #region IQueryCriteria<T> Members

        public IQueryCriteria<T> AddConjunctionQueryCriteria(IQueryCriteria<T> queryCriteria)
        {
            if (queryCriteria == null)
                throw new ArgumentNullException();

            _conjuntionInnerCriteriaList.Add(queryCriteria);

            return this;
        }

        public IQueryCriteria<T> AddDisjunctionQueryCriteria(IQueryCriteria<T> queryCriteria)
        {
            if (queryCriteria == null)
                throw new ArgumentNullException();

            _disjuntionInnerCriteriaList.Add(queryCriteria);

            return this;
        }

        public IQueryCriteria<T> AddConjunctionPredicate(IQueryPredicate predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException();

            _conjunctionList.Add(predicate);

            return this;
        }

        public IQueryCriteria<T> AddDisjunctionPredicate(IQueryPredicate predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException();

            _disjunctionList.Add(predicate);

            return this;
        }
                
        public IQueryCriteria<T> AddOrder(IQueryOrder order)
        {
            if (order == null)
                throw new ArgumentNullException();
                        
            _orderList.Add(order);

            return this;
        }

        public void Accept(IQueryCriteriaVisitor<T> visitor)
        {
            visitor.VisitCriteria(this._conjuntionInnerCriteriaList,
                this._disjuntionInnerCriteriaList,
                this._conjunctionList, 
                this._disjunctionList, 
                this._orderList);
        }

        #endregion
    }
}
