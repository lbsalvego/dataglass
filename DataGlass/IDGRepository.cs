﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DataGlass
{
    /// <summary>
    /// Represents a mediator between the domain layer and the data mapping layer
    /// </summary>
    /// <typeparam name="T">Domain Entity type</typeparam>
    public interface IDGRepository<T> where T : class
    {
        /// <summary>
        /// Obtains an instance of Domain Entity
        /// </summary>
        /// <param name="id">Entity integer ID</param>
        T GetById(int id);

        /// <summary>
        /// Obtains an instance of Domain Entity
        /// </summary>
        /// <param name="id">Entity ID</param>
        T GetById(object objectId);

        /// <summary>
        /// Obtains an instance of a proxy to a Domain Entity
        /// </summary>
        /// <param name="id">Entity integer ID</param>
        T LoadById(int id);

        /// <summary>
        /// Obtains an instance of a proxy to a Domain Entity
        /// </summary>
        /// <param name="id">Entity ID</param>
        T LoadById(object objectId);

        /// <summary>
        /// Obtains a list of Domain Entity
        /// </summary>
        /// <param name="query">String Query to be executed</param>
        /// <param name="parameters">Additional parameter</param>
        /// <returns>List of Entity instances</returns>
        IList<T> GetByStringQuery(string query, params object[] parameters);

        /// <summary>
        /// Obtains a list of Domain Entity
        /// </summary>
        /// <param name="query">DGQuery to be executed</param>
        IList<T> GetByDGQuery(IDGQueryCriteria<T> query);

        /// <summary>
        /// Obtains a list of Domain Entity
        /// </summary>
        /// <param name="queryName">Named Query to be executed</param>
        /// <param name="parameters">Additional parameter</param>
        /// <returns>List of Entity instances</returns>
        IList<T> GetByNamedQuery(string queryName, params object[] parameters);

        /// <summary>
        /// Obtains a queryable list of Domain Entity
        /// </summary>
        /// <returns>Queryable list of Entity instances</returns>
        IQueryable<T> GetQueryable();

        /// <summary>
        /// Obtains a lazy load queryable list of Domain Entity
        /// </summary>
        /// <returns>Lazy Queryable list of Entity instances</returns>
        IQueryable<T> GetLazyQueryable();

        /// <summary>
        /// Persists the new entity
        /// </summary>
        /// <param name="entity">New entity</param>
        void Save(T entity);

        /// <summary>
        /// Persiste the entity
        /// </summary>
        /// <param name="entity">Entity</param>
        void SaveOrUpdate(T entity);

        /// <summary>
        /// Deletes the entity from database
        /// </summary>
        /// <param name="entity">Entity</param>
        void Delete(T entity);

        /// <summary>
        /// Evicts an entity
        /// </summary>
        /// <param name="entity">Entity</param>
        void Evict(T entity);

        /// <summary>
        /// Detachs an entity
        /// </summary>
        /// <param name="entity">Entity</param>
        void Detach(T entity);

        /// <summary>
        /// Attachs an entity
        /// </summary>
        /// <param name="entity">Entity</param>
        void Attach(T entity);

        /// <summary>
        /// Verifies if the Repository contains the instance of the entity
        /// </summary>
        /// <param name="entity">Entity</param>
        bool Contains(T entity);

        /// <summary>
        /// Refreshes an entity with data from database
        /// </summary>
        /// <param name="entity">Entity</param>
        void Refresh(T entity);
    }
}
