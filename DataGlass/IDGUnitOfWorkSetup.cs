﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass
{
    /// <summary>
    /// Represents a Setup object for an Unit of Work
    /// </summary>
    public interface IDGUnitOfWorkSetup
    {
        /// <summary>
        /// Setup class is set
        /// </summary>
        bool IsSet
        {
            get;
        }
        
        /// <summary>
        /// Setup the Unit of Work
        /// </summary>
        void Setup();
    }
}
