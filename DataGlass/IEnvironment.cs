﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass
{
    /// <summary>
    /// Represents an Environment
    /// </summary>
    public interface IEnvironment
    {
        /// <summary>
        /// Relational Mapping Mechanism Name
        /// </summary>
        string MechanismName
        { 
            get; 
        }

        /// <summary>
        /// Relational Mapping Mechanism DLL Name
        /// </summary>
        string MechanismDllName
        {
            get;
        }

        /// <summary>
        /// Obtains an Unit Of Work
        /// </summary>
        IUnitOfWork GetUnitOfWork();
    }
}
