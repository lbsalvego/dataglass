﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass
{
    /// <summary>
    /// Represents a terminal predicate for a query restriction
    /// </summary>
    public interface IDGQueryPredicate
    {
        /// <summary>
        /// Predicate field
        /// </summary>
        string Field
        {
            get;
        }

        /// <summary>
        /// Predicate value
        /// </summary>
        string Value
        {
            get;
        }

        /// <summary>
        /// Predicate operator
        /// </summary>
        PredicateOperator Operator
        {
            get;
        }
    }
}
