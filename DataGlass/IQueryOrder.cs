﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass
{
    /// <summary>
    /// Represents a query order
    /// </summary>
    public interface IQueryOrder
    {
        /// <summary>
        /// Order field
        /// </summary>
        string Field
        {
            get;
        }

        /// <summary>
        /// Order operator
        /// </summary>
        OrderOperator Operator
        {
            get;
        }
    }
}
