﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass
{
    /// <summary>
    /// Represents unanbiguos criteria for a query
    /// </summary>
    /// <typeparam name="T">Domain Entity type</typeparam>
    public interface IDGQueryCriteria<T> where T : class
    {
        /// <summary>
        /// Adds an conjunction (AND) inner criteria to the query
        /// </summary>
        /// <param name="queryCriteria">Inner query criteria</param>
        IDGQueryCriteria<T> AddConjunctionQueryCriteria(IDGQueryCriteria<T> queryCriteria);

        /// <summary>
        /// Adds an disjunction (OR) inner criteria to the query
        /// </summary>
        /// <param name="queryCriteria">Inner query criteria</param>
        IDGQueryCriteria<T> AddDisjunctionQueryCriteria(IDGQueryCriteria<T> queryCriteria);

        /// <summary>
        /// Adds a conjuntion predicate (AND where statement) to the query
        /// </summary>
        /// <param name="predicate">Predicate</param>
        IDGQueryCriteria<T> AddConjunctionPredicate(IDGQueryPredicate predicate);

        /// <summary>
        /// Adds a disjuntion predicate (OR where statement) to the query
        /// </summary>
        /// <param name="predicate">Predicate</param>
        IDGQueryCriteria<T> AddDisjunctionPredicate(IDGQueryPredicate predicate);

        /// <summary>
        /// Adds an order (order by statement) to the query
        /// </summary>
        /// <param name="order">Order</param>
        IDGQueryCriteria<T> AddOrder(IDGQueryOrder order);

        /// <summary>
        /// Accepts a visitor to analyze the query criteria
        /// </summary>
        /// <param name="visitor">Query criteria visitor</param>
        void Accept(IDGQueryCriteriaVisitor<T> visitor);
    }
}
