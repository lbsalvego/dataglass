﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass
{
    public class QueryPredicate : IQueryPredicate
    {
        #region Private Fields

        private string _field;
        private string _value;
        private PredicateOperator _operator;

        #endregion

        #region Private Methods
        
        private QueryPredicate(string field, string value, PredicateOperator predicateOperator)
        {
            _field = field;
            _value = value;
            _operator = predicateOperator;
        }

        #endregion

        #region Public Methods

        public static QueryPredicate Equals(string field, string value)
        {
            return new QueryPredicate(field, value, PredicateOperator.Equals);
        }

        public static QueryPredicate NotEquals(string field, string value)
        {
            return new QueryPredicate(field, value, PredicateOperator.NotEquals);
        }

        public static QueryPredicate Greater(string field, string value)
        {
            return new QueryPredicate(field, value, PredicateOperator.Greater);
        }

        public static QueryPredicate GreaterOrEquals(string field, string value)
        {
            return new QueryPredicate(field, value, PredicateOperator.GreaterOrEquals);
        }

        public static QueryPredicate Less(string field, string value)
        {
            return new QueryPredicate(field, value, PredicateOperator.Less);
        }

        public static QueryPredicate LessOrEquals(string field, string value)
        {
            return new QueryPredicate(field, value, PredicateOperator.LessOrEquals);
        }

        public static QueryPredicate Like(string field, string value)
        {
            return new QueryPredicate(field, value, PredicateOperator.Like);
        }

        #endregion

        #region IQueryPredicate Members

        public string Field
        {
            get { return _field; }
        }

        public string Value
        {
            get { return _value; }
        }

        public PredicateOperator Operator
        {
            get { return _operator; }
        }

        #endregion
    }
}
