﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DataGlass
{
    /// <summary>
    /// Represents a business transaction
    /// </summary>
    public interface IDGUnitOfWork : IDisposable
    {
        /// <summary>
        /// Number of transactions currently running on the current Unit of Work
        /// </summary>
        int TransactionCounter
        {
            get;
        }

        /// <summary>
        /// Begins a transaction within a Unit of Work
        /// </summary>
        void BeginTransaction();

        /// <summary>
        /// Begins a transaction within a Unit of Work
        /// </summary>
        void BeginTransaction(IsolationLevel isolationLevel);

        /// <summary>
        /// Commits a transaction within a Unit of Work
        /// </summary>
        void Commit();

        /// <summary>
        /// Rollbacks a transaction within a Unit of Work
        /// </summary>
        void Rollback();

        /// <summary>
        /// Obtains the current database connection
        /// </summary>
        IDbConnection GetConnection();

        /// <summary>
        /// Enlists a SQL command to be part of the current Unit Of Work's current transaction
        /// </summary>
        /// <param name="command"></param>
        void Enlist(IDbCommand command);

        /// <summary>
        /// Cleans all Cache
        /// </summary>
        void ClearCache();

        /// <summary>
        /// Returns a Repository for a Domain Entity  
        /// </summary>
        /// <typeparam name="T">Domain Entity type</typeparam>
        IDGRepository<T> GetRepository<T>() where T: class;
    }
}
