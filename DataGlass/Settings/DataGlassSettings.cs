﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace DataGlass.Settings
{
    public class DataGlassSettings : ConfigurationSection
    {
        private static DataGlassSettings settings = ConfigurationManager.GetSection("DataGlassSettings") as DataGlassSettings;

        public static DataGlassSettings Settings
        {
            get
            {
                return settings;
            }
        }

        /// <summary>
        /// Relational Mapping Mechanism Assembly
        /// </summary>
        [ConfigurationProperty("relationalMappingMechanism", IsRequired=true)]
        public string RelationalMappingMechanism
        {
            get { return (string)this["relationalMappingMechanism"]; }
            set { this["relationalMappingMechanism"] = value; }
        }
    }
}
