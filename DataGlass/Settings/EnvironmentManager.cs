﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace DataGlass.Settings
{
    /// <summary>
    /// Factory class to create Environment objects
    /// </summary>
    public static class EnvironmentManager
    {
        #region Private Fields

        private static IDGEnvironment _environment;

        #endregion

        #region Private Methods

        /// <summary>
        /// Attempts to load an Environment directly from the DLL
        /// </summary>
        /// <param name="dll">DLL name</param>
        private static IDGEnvironment LoadEnvironmentFromDll(string dll)
        {
            IDGEnvironment environment = null;
            var assembly = Assembly.LoadFrom(dll);
            if (assembly != null)
            {
                var types = assembly.GetExportedTypes();
                foreach (var type in types)
                {
                    if ((typeof(IDGEnvironment).IsAssignableFrom(type)) && (!type.IsAbstract))
                    {
                        environment = assembly.CreateInstance(type.FullName) as IDGEnvironment;
                        break;
                    }
                }
            }

            if (environment == null)
                throw new ApplicationException(string.Format("Environment not implemented in {0}.", dll));

            return environment;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns an Environment object based on configuration options
        /// </summary>
        public static IDGEnvironment GetEnvironment()
        {
            if (_environment == null)
            {
                var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
                if (path.ToLower().StartsWith("file:\\"))
                    path = path.Remove(0, 6);

                var relationalMappingMechanism = DataGlassSettings.Settings.RelationalMappingMechanism.ToUpper();

                _environment = LoadEnvironmentFromDll(Path.Combine(path, relationalMappingMechanism));
            }

            return _environment;
        }
        
        /// <summary>
        /// Cleans the Environment 
        /// </summary>
        public static void ClearEnvironment()
        {
            _environment = null;
        }

        #endregion
    }
}
