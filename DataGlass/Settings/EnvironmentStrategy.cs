﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;

namespace DataGlass.Settings
{
    /// <summary>
    /// Implements an strategy to load the correct Environment dinamicaly
    /// </summary>
    internal class EnvironmentStrategy
    {
        /// <summary>
        /// Returns an Environment object based on configuration options
        /// </summary>
        public IEnvironment GetEnvironment()
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            if (path.ToLower().StartsWith("file:\\"))
                path = path.Remove(0, 6);
            
            var relationalMappingMechanism = DataGlassSettings.Settings.RelationalMappingMechanism.ToUpper();

            return this.LoadEnvironmentFromDll(Path.Combine(path, relationalMappingMechanism));
        }

        /// <summary>
        /// Attempts to load an Environment directly from the DLL
        /// </summary>
        /// <param name="dll">DLL name</param>
        private IEnvironment LoadEnvironmentFromDll(string dll)
        {
            IEnvironment environment = null;
            var assembly = Assembly.LoadFrom(dll);
            if (assembly != null)
            {
                var types = assembly.GetExportedTypes();
                foreach (var type in types)
                {
                    if ((typeof(IEnvironment).IsAssignableFrom(type)) && (!type.IsAbstract))
                    {
                        environment = assembly.CreateInstance(type.FullName) as IEnvironment;
                        break;
                    }
                }
            }

            if (environment == null)
                throw new ApplicationException(string.Format("Environment not implemented in {0}.", dll));

            return environment;
        }
    }
}
