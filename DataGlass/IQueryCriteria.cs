﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass
{
    /// <summary>
    /// Represents unanbiguos criteria for a query
    /// </summary>
    /// <typeparam name="T">Domain Entity type</typeparam>
    public interface IQueryCriteria<T> where T : class
    {
        /// <summary>
        /// Adds an conjunction (AND) inner criteria to the query
        /// </summary>
        /// <param name="queryCriteria">Inner query criteria</param>
        IQueryCriteria<T> AddConjunctionQueryCriteria(IQueryCriteria<T> queryCriteria);

        /// <summary>
        /// Adds an disjunction (OR) inner criteria to the query
        /// </summary>
        /// <param name="queryCriteria">Inner query criteria</param>
        IQueryCriteria<T> AddDisjunctionQueryCriteria(IQueryCriteria<T> queryCriteria);

        /// <summary>
        /// Adds a conjuntion predicate (AND where statement) to the query
        /// </summary>
        /// <param name="predicate">Predicate</param>
        IQueryCriteria<T> AddConjunctionPredicate(IQueryPredicate predicate);

        /// <summary>
        /// Adds a disjuntion predicate (OR where statement) to the query
        /// </summary>
        /// <param name="predicate">Predicate</param>
        IQueryCriteria<T> AddDisjunctionPredicate(IQueryPredicate predicate);

        /// <summary>
        /// Adds an order (order by statement) to the query
        /// </summary>
        /// <param name="order">Order</param>
        IQueryCriteria<T> AddOrder(IQueryOrder order);

        /// <summary>
        /// Accepts a visitor to analyze the query criteria
        /// </summary>
        /// <param name="visitor">Query criteria visitor</param>
        void Accept(IQueryCriteriaVisitor<T> visitor);
    }
}
