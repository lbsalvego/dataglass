﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass
{
    public class DGQueryPredicate : IDGQueryPredicate
    {
        #region Private Fields

        private string _field;
        private string _value;
        private PredicateOperator _operator;

        #endregion

        #region Private Methods
        
        private DGQueryPredicate(string field, string value, PredicateOperator predicateOperator)
        {
            _field = field;
            _value = value;
            _operator = predicateOperator;
        }

        #endregion

        #region Public Methods

        public static DGQueryPredicate Equals(string field, string value)
        {
            return new DGQueryPredicate(field, value, PredicateOperator.Equals);
        }

        public static DGQueryPredicate NotEquals(string field, string value)
        {
            return new DGQueryPredicate(field, value, PredicateOperator.NotEquals);
        }

        public static DGQueryPredicate Greater(string field, string value)
        {
            return new DGQueryPredicate(field, value, PredicateOperator.Greater);
        }

        public static DGQueryPredicate GreaterOrEquals(string field, string value)
        {
            return new DGQueryPredicate(field, value, PredicateOperator.GreaterOrEquals);
        }

        public static DGQueryPredicate Less(string field, string value)
        {
            return new DGQueryPredicate(field, value, PredicateOperator.Less);
        }

        public static DGQueryPredicate LessOrEquals(string field, string value)
        {
            return new DGQueryPredicate(field, value, PredicateOperator.LessOrEquals);
        }

        public static DGQueryPredicate Like(string field, string value)
        {
            return new DGQueryPredicate(field, value, PredicateOperator.Like);
        }

        #endregion

        #region IQueryPredicate Members

        public string Field
        {
            get { return _field; }
        }

        public string Value
        {
            get { return _value; }
        }

        public PredicateOperator Operator
        {
            get { return _operator; }
        }

        #endregion
    }
}
