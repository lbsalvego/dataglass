﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass
{
    public enum PredicateOperator { Equals, NotEquals, Greater, GreaterOrEquals, Less, LessOrEquals, Like };

    public enum OrderOperator { Ascending, Descending };
}
