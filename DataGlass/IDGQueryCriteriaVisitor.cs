﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGlass
{
    /// <summary>
    /// Represents a visitor for a query criteria
    /// </summary>
    /// <typeparam name="T">Domain Entity type</typeparam>
    public interface IDGQueryCriteriaVisitor<T> where T : class
    {
        /// <summary>
        /// Visits a query criteria for analysis
        /// </summary>
        /// <param name="conjunctionInnerCriterias">List of conjunction inner criterias</param>
        /// <param name="disjunctionInnerCriterias">List of disjunction inner criterias</param>
        /// <param name="conjunctions">List of conjunction predicates</param>
        /// <param name="disjunctions">List of disjunction predicates</param>
        /// <param name="orders">List of orders</param>
        void VisitCriteria(List<IDGQueryCriteria<T>> conjunctionInnerCriterias, List<IDGQueryCriteria<T>> disjunctionInnerCriterias, List<IDGQueryPredicate> conjunctions, List<IDGQueryPredicate> disjunctions, List<IDGQueryOrder> orders);
    }
}
